import { Food } from "../../models/v1/model-v1.js";
import showInfo, { getFormData } from "./controller-v1.js";

document.getElementById("btnThemMon").onclick = () => {
  let data = getFormData();
  console.log("🚀 ~ data:", data);

  let { id, name, type, price, discount, status, img, desc } = data;
  let food = new Food(id, name, type, price, discount, status, img, desc);

  console.log("🚀 ~ food:", food);
  showInfo(food);
};
