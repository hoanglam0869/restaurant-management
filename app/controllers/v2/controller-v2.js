export let renderFoodList = (foodList) => {
  document.getElementById("tbodyFood").innerHTML = foodList
    .map(
      (food) => `
    <tr>
        <td>${food.id}</td>
        <td>${food.name}</td>
        <td>${food.type ? "Chay" : "Mặn"}</td>
        <td>${food.price}</td>
        <td>${food.discount}</td>
        <td>0</td>
        <td>${food.status ? "Còn" : "Hết"}</td>
        <td>
            <button class="btn btn-danger" onclick="deleteFood(${
              food.id
            })">Xóa</button>
            <button class="btn btn-success" onclick="editFood(${
              food.id
            })">Sửa</button>
        </td>
    </tr>`
    )
    .join("");
};

export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    destination: "https://github.com/apvarun/toastify-js",
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
};

let setFormData = (food) => {
  document.getElementById("foodID").value = food.id;
  document.getElementById("tenMon").value = food.name;
  document.getElementById("loai").value = food.type ? "loai1" : "loai2";
  document.getElementById("giaMon").value = food.price;
  document.getElementById("khuyenMai").value = "10";
  document.getElementById("tinhTrang").value = food.status ? "1" : "0";
  document.getElementById("hinhMon").value = food.img;
  document.getElementById("moTa").value = food.desc;
};

export default setFormData;
