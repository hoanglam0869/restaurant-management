export const getFormData = () => {
  let id = document.getElementById("foodID").value;
  let name = document.getElementById("tenMon").value;
  let type = document.getElementById("loai").value == "loai1" ? true : false;
  let price = document.getElementById("giaMon").value;
  let discount = document.getElementById("khuyenMai").value;
  let status = document.getElementById("tinhTrang").value == 0 ? false : true;
  let img = document.getElementById("hinhMon").value;
  let desc = document.getElementById("moTa").value;

  return {
    id: id,
    name: name,
    type: type,
    price: price,
    discount: discount,
    status: status,
    img: img,
    desc: desc,
  };
};

let setFormData = (food) => {
  console.log("🚀 ~ setFormData ~ food:", food);
  document.getElementById("imgMonAn").src = food.img;
  document.getElementById("spMa").innerText = food.id;
  document.getElementById("spTenMon").innerText = food.name;
  document.getElementById("spLoaiMon").innerText = food.type ? "Chay" : "Mặn";
  document.getElementById("spGia").innerText = Intl.NumberFormat().format(
    food.price
  );
  document.getElementById("spKM").innerText = `${food.discount}%`;
  document.getElementById("spGiaKM").innerText = Intl.NumberFormat().format(
    (food.price * (100 - food.discount)) / 100
  );
  document.getElementById("spTT").innerText = food.status ? "Còn" : "Hết";
  document.getElementById("pMoTa").innerText = food.desc;
};

export default setFormData;
