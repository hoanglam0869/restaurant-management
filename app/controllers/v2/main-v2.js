import showInfo, { onSuccess, renderFoodList } from "./controller-v2.js";
import { getFormData } from "../v1/controller-v1.js";

const BASE_URL = "https://63f442d8864fb1d60024f92d.mockapi.io";

let fetchFoodList = (key) => {
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then((res) => {
      console.log("🚀 ~ res:", res.data);
      if (key == "all") {
        renderFoodList(res.data);
      } else {
        let type = key == "loai1" ? true : false;
        let filterList = res.data.filter((food) => food.type == type);
        renderFoodList(filterList);
      }
    })
    .catch((err) => {
      console.log("🚀 ~ err:", err);
    });
};

fetchFoodList("all");

let deleteFood = (id) => {
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      console.log("🚀 ~ res:", res.data);
      onSuccess("Xóa thành công");
      fetchFoodList("all");
    })
    .catch((err) => {
      console.log("🚀 ~ err:", err);
    });
};
window.deleteFood = deleteFood;

let createFood = () => {
  let data = getFormData();
  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data,
  })
    .then((res) => {
      console.log("🚀 ~ res:", res.data);
      $("#exampleModal").modal("hide");
      onSuccess("Thêm thành công");
      fetchFoodList("all");
    })
    .catch((err) => {
      console.log("🚀 ~ err:", err);
    });
};
window.createFood = createFood;

window.editFood = (id) => {
  console.log("🚀 ~ id:", id);
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log("🚀 ~ res:", res.data);
      $("#exampleModal").modal("show");
      showInfo(res.data);
    })
    .catch((err) => {
      console.log("🚀 ~ err:", err);
    });
};

document.getElementById("btnCapNhat").onclick = () => {
  let data = getFormData();
  axios({
    url: `${BASE_URL}/food/${data.id}`,
    method: "PUT",
    data,
  })
    .then((res) => {
      console.log("🚀 ~ res:", res.data);
      $("#exampleModal").modal("hide");
      onSuccess("Cập nhật thành công");
      fetchFoodList("all");
    })
    .catch((err) => {
      console.log("🚀 ~ err:", err);
    });
};

document.getElementById("selLoai").onchange = () => {
  let type = document.getElementById("selLoai").value;
  fetchFoodList(type);
};
