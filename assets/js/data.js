const list = [
  {
    id: "1",
    name: "Bún gạo xào",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/1a51154b-9469-43ff-a25e-0adc7f47-6da28b9f-201124114223.jpeg",
    desc: "Bún gạo xào",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "2",
    name: "Mì xào giòn chay",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/d4e26f43-10e4-4b59-84bd-ab74a7de-a7b1c508-211119132901.jpeg",
    desc: "Mì xào giòn chay",
    price: 40000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "3",
    name: "Lẩu Thái chay",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/f366ad7f-50e4-4773-9a43-720e7d3f-27fe8b2d-220414173506.jpeg",
    desc: "Lẩu Thái chay",
    price: 150000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "4",
    name: "Cơm chay",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/552283be-230a-45b0-b62e-fb99ddd6-878e9a78-201124112542.jpeg",
    desc: "Cơm chay",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "5",
    name: "Bún riêu chay",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/cd2d7c84-0406-480d-b304-fa9c8777-d733f1ee-201124113900.jpeg",
    desc: "Bún riêu chay",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "6",
    name: "Cà ri nấm",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/a7417cff-91e1-4c67-810e-c9cd9f1f-e973acfa-201124112805.jpeg",
    desc: "Cà ri nấm",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "7",
    name: "Bánh ướt chay",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/9d65a46f-8b2d-4227-87d6-e17c57eb-132b1d00-211124112429.jpeg",
    desc: "Bánh ướt chay",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "8",
    name: "Bún chả giò",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/a21e13ec-7aad-4352-ba1e-f5df8a98-a3bdf58a-201124114002.jpeg",
    desc: "Bún chả giò",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "9",
    name: "Bột chiên trứng",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/475e14fc-dc9c-43f8-a43b-507b0e81-51df6d24-221115110121.jpeg",
    desc: "Bột chiên trứng",
    price: 30000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "10",
    name: "Nui xào rau củ",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/6c53fad9-06db-43d3-ad94-e3c4aa82-8a1cb557-220414173644.jpeg",
    desc: "Nui xào rau củ",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "11",
    name: "Mì xào rau củ",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/2c54a1ad-48fd-4dc4-aeed-1024b140-61091462-220414173609.jpeg",
    desc: "Mì xào rau củ",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "12",
    name: "Hủ tíu soup",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/150350cd-0532-45ec-a80d-93a6dbf9-9dc25b38-201124113412.jpeg",
    desc: "Hủ tíu soup",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "13",
    name: "Bánh canh",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/05c61e56-8c63-4c61-ad42-fcbc7d0b-869e0802-201124113458.jpeg",
    desc: "Bánh canh",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "14",
    name: "Bún gạo nước soup",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/a2b61119-125a-452b-81ff-ed359539-764b908e-201124113713.jpeg",
    desc: "Bún gạo nước soup",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "15",
    name: "Canh cải nấu nấm",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/bb5b12ab-35f9-40b7-93cf-7e0e5601-2f0e3c01-200903111628.jpeg",
    desc: "Canh cải nấu nấm",
    price: 10000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "16",
    name: "Đậu hũ miếng chiên sả",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/cd04e07f-12a3-47a4-b54e-258a9d8b-81e74b59-220414174212.jpeg",
    desc: "Đậu hũ miếng chiên sả",
    price: 13000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "17",
    name: "Sườn non chiên sả",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/f7ba1fce-c223-4f37-a19a-8c59848d-1e408048-220414174401.jpeg",
    desc: "Sườn non chiên sả",
    price: 15000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "18",
    name: "Đậu hũ kho nấm",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/9e1904b3-b367-4623-b5cc-96c969d4-aac3c1d6-220414174133.jpeg",
    desc: "Đậu hũ kho nấm",
    price: 20000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "19",
    name: "Rau luộc chấm chao",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/e0fe72dd-2557-4e26-9587-5c10142b-e6165bb6-220414174337.jpeg",
    desc: "Rau luộc chấm chao",
    price: 30000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "20",
    name: "Rau xào nấm",
    type: true,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g104/1039356/s400x400/41b0058c-59e0-4218-ac3c-8ef0f40b-5aaf4802-220414174319.jpeg",
    desc: "Rau xào nấm",
    price: 20000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "21",
    name: "Mì tôm trộn thịt bằm",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/97630fd6-ef66-445e-bc50-3b0b7dba-415b038d-220929002143.jpeg",
    desc: "Mì tôm trộn thịt bằm",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "22",
    name: "Xúc xích",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/18022436-f58a-4a41-81e4-05d53690-1e007f99-211229232136.jpeg",
    desc: "Xúc xích",
    price: 15000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "23",
    name: "Đậu hũ phomai tròn",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/5cceb356-5386-492d-95b0-47fca674-dd69be22-211115214757.jpeg",
    desc: "Đậu hũ phomai tròn",
    price: 20000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "24",
    name: "Cá viên chiên",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/0bb42d36-42c8-4355-9670-a8535ad3-fc2e4639-211105155200.jpeg",
    desc: "Cá viên chiên",
    price: 24000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "25",
    name: "Cốm hồng sốt Mayonaise",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/f3c29c04-c0b6-43ed-b563-fa20a3f2-3b3f6051-211130190600.jpeg",
    desc: "Cốm hồng sốt Mayonaise",
    price: 15000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "26",
    name: "Xúc xích lốc xoáy",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/98ff9e6c-e2b9-4d55-9b57-3a5bdfe1-af456a9e-211130191443.jpeg",
    desc: "Xúc xích lốc xoáy",
    price: 13000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "27",
    name: "Hồ lô Thái",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/09f80ac3-c829-4a0d-a2d8-3caf9f61-496bc288-211105155627.jpeg",
    desc: "Hồ lô Thái",
    price: 12000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "28",
    name: "Bò viên",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/9af5038e-6f18-41df-975b-e1d150ed-395f4e4d-211124030908.jpeg",
    desc: "Bò viên",
    price: 24000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "29",
    name: "Cá viên trứng cút",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/f8b9a8eb-97cb-44a0-8321-0972c4eb-cbc910e1-211105155331.jpeg",
    desc: "Cá viên trứng cút",
    price: 13000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "30",
    name: "Thập cẩm",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/d91e7cf4-e94a-4b9d-becf-44417b55-b67861dc-211115230342.jpeg",
    desc: "Thập cẩm",
    price: 100000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "31",
    name: "Phomai que",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/0c2a7ae3-ee35-480e-a694-7dd6e4d4-0e9596ed-211130190306.jpeg",
    desc: "Phomai que",
    price: 14000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "32",
    name: "Tôm viên",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/9007fbcf-acf4-415b-98a8-e784b566-d4f95285-211115215007.jpeg",
    desc: "Tôm viên",
    price: 12000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "33",
    name: "Chả cá đậu hũ",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/6bca7e68-d08b-4fc4-a1b8-3e5080d5-629edba7-211115214720.jpeg",
    desc: "Chả cá đậu hũ",
    price: 15000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "34",
    name: "Hoành thánh",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/201fb189-991d-4995-805a-cc219127-25c34f17-220308073950.jpeg",
    desc: "Hoành thánh",
    price: 24000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "35",
    name: "Ốc nhồi",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/ac3e07c5-2255-44f2-bc64-cbff17ed-cf43656d-211105155914.jpeg",
    desc: "Ốc nhồi",
    price: 12000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "36",
    name: "Chả cá cốm xanh",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/a8acb66e-38ea-480e-bdd2-421df13d-4ef9e76d-211105160053.jpeg",
    desc: "Chả cá cốm xanh",
    price: 12000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "37",
    name: "Bánh bao trứng cá cam",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/e7a3a9b8-1732-48df-9090-0a740eb3-ad5928f1-220116121200.jpeg",
    desc: "Bánh bao trứng cá cam",
    price: 12000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "38",
    name: "Bánh bao trứng muối xanh",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g100004/1000036500/s400x400/fa96035d-af71-4454-861f-26f7b42b-4f9b2bd2-211230205502.jpeg",
    desc: "Bánh bao trứng muối xanh",
    price: 15000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "39",
    name: "Cháo thịt bằm",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g114/1137704/s400x400/137f61f4-bca5-4674-bbb1-7ce1de92-41bc4567-220523141001.jpeg",
    desc: "Cháo thịt bằm",
    price: 35000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "40",
    name: "Bún riêu",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g114/1137704/s400x400/d9cbd450-0e3a-44da-8a01-43fcfd24-239d43d7-220523140904.jpeg",
    desc: "Bún riêu",
    price: 32000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "41",
    name: "Cháo trắng hột vịt muối",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g114/1137704/s400x400/fbb92cee-de69-4d18-9de0-3673a527-90b952d8-220523141536.jpeg",
    desc: "Cháo trắng hột vịt muối",
    price: 25000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "42",
    name: "Súp cua",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g114/1137704/s400x400/8baeb216-d1eb-4317-a0f6-b9820bf4-2aee10bd-220715171513.jpeg",
    desc: "Súp cua",
    price: 30000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "43",
    name: "Cháo trắng chà bông",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g114/1137704/s400x400/a4fe97c2-751b-4d22-8bed-ed578007-8df03102-220523141012.jpeg",
    desc: "Cháo trắng chà bông",
    price: 25000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "44",
    name: "Bánh tráng trộn",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g114/1137704/s400x400/55ec7ae5-73fb-4d5e-9245-8de2965b-4605be11-220715171925.jpeg",
    desc: "Bánh tráng trộn",
    price: 25000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "45",
    name: "Súp thập cẩm",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g114/1137704/s400x400/6872d3dd-bd5b-405c-aef0-71c65a60-d31569da-220817122545.jpeg",
    desc: "Súp thập cẩm",
    price: 43000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
  {
    id: "46",
    name: "Cháo trắng hột vịt bắc thảo",
    type: false,
    discount: getRndInteger(1, 2) * 10,
    img: "https://images.foody.vn/res/g114/1137704/s400x400/63d9b197-86b2-45ab-86cb-35411fde-dc999b57-220523141504.jpeg",
    desc: "Cháo trắng hột vịt bắc thảo",
    price: 28000,
    status: getRndInteger(0, 1) == 0 ? false : true,
  },
];

function getRndInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

let createRandomData = () => {
  let index = getRndInteger(0, list.length - 1);

  document.getElementById("foodID").value = list[index].id;
  document.getElementById("tenMon").value = list[index].name;
  document.getElementById("loai").value = list[index].type ? "loai1" : "loai2";
  document.getElementById("giaMon").value = list[index].price;
  document.getElementById("khuyenMai").value = list[index].discount;
  document.getElementById("tinhTrang").value = list[index].status ? "1" : "0";
  document.getElementById("hinhMon").value = list[index].img;
  document.getElementById("moTa").value = list[index].desc;
};

document.querySelector("h5").onclick = () => createRandomData();
let label = document.getElementById("exampleModalLabel");
if (label != null) {
  label.onclick = () => createRandomData();
}
